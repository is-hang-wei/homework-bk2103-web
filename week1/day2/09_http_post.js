const http = require('http')
const path = require('path')
const fs = require('fs')
const url = require('url')
const qs = require('querystring')

http.createServer((req,res) => {

    if('/favicon.ico' != req.url) {

        if(req.method == 'POST') {
            let postData = ''

            req.on('data' , chunk => {
                postData += chunk
            })

            req.on('end', () => {
                console.log(qs.parse(postData));
            })
        }
    }
    res.end('aaab');

}).listen(3000,'127.0.0.1' , () => console.log('服务器启动'))