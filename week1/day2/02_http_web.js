// nodejs是一个事件驱动的
// 使用nodejs内置的http模块完成
const http = require('http')
// 得到一个webserver对象
const server = http.createServer();

// 监听客户发过来的请求
server.on('request',(request,response) => {
    


    console.log('客户端有请求');

    // 在localhost:3000上打开，打印出aaa
    // 服务器对客户端本次请求的响应
    // response.end("aaabbb")

    // 获取当前的url地址，不带协议和域名
    console.log(request.url);
    // 获取当前的请求方法 GET/POST 返回的字符串是大写
    console.log(request.method);
    // 获取请求头信息  得到的是一个对象
    console.log(request.headers);
    // 浏览器型号  有中横线的
    console.log(request.headers['user-agent']);

    response.setHeader('content-type','text/html;charset=utf-8')
    response.end('你好世界');

    // 15分钟的时候出现两次请求
    // 是icon图标的影响

    
    


});

// 绑定主机和端口
server.listen(3000,'127.0.0.1',() => console.log('服务器启动'))

