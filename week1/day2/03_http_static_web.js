// 导入http内置模块
const http = require('http')
// 导入path内置模块
const path = require('path')
// 导入fs内置模块
const fs = require('fs')

// 生成webserver对象
const server = http.createServer();

// 定义  对应的mime类型
// let mimeTypes = {
//     'html' : 'text/html;charset=utf-8',
//     'jpg' : 'image/jpeg',
//     'css' : 'text/css',
//     'js' : 'application/x-javascript'
// }
let mimeTypes = require('./libs/mime')

//定义网站根目录  相对转绝对
const webRoot = path.resolve('public')

// 监听客户端发来的请求
server.on('request',(req,res) => {
    // 获取请求地址  不带协议名和路径 做缺省值处理
    let pathname = req.url == '/' ? '/index.html' : req.url

    // 对icon请求不做任何处理
    if(pathname != '/favicon.ico'){
        // 请求的url地址和本机服务器中存在的文件进行映射  得到访问文件绝对路径
        var filepath = path.join(webRoot,pathname)

        // 得到  路径中的  扩展名
        let extname = path.extname(filepath).slice(1);
        /**短语 */
        let mime = mimeTypes[extname] || 'text/html;charset=utf-8'
        /** 拼接出来没问题，但是不能确定文件存不存在
         * so...
         * 判断文件是否存在
         * */ 

        if(fs.existsSync(filepath)){ //存在
            // 设置响应头中的mime类型
            res.setHeader('content-type' , mime)
            // 读取磁盘中存在的文件  显示
            res.end(fs.readFileSync(filepath))
        } else { //不存在
            // 404页面路径
            var filepath = path.join(webRoot,'/404.html')
            
            // 设置响应头中的mime类型
            res.setHeader('content-type' , mime)

            //响应状态码
            res.statusCode = 404
            // 服务器对客户端本次请求的响应
            // 404页面
            res.end(fs.readFileSync(filepath))
        }
    }
});

server.listen(3000,'127.0.0.1',() => console.log('服务器启动'))