#### 1、web服务器

请求：

  请求行、请求头、请求体(get)

响应：

  响应行、响应头、响应体(204)

#### 2、页面渲染模式

ssr(服务器端渲染)

csr(客户端渲染)



创建web服务基本步骤
①、导入nodejs内置的http模块   

```javascript
const http = require('http')
```

②、创建web服务对象实例

```javascript
const server = http.createServer()
```

③、绑定监听客户端请求事件request

```javascript
server.on('request', (request, response) => {})
```

​	

	request: 接受客户端请求对象，它包含了与客户端相关的数据和属性
			   request.url      客户端请求的uri地址
			   request.method  客户端请求的方式 get或post
			   req.headers	  客户端请求头信息
	response:服务器对客户端的响应对象
			  # 设置响应头信息 ，用于响应时有中文时乱码解决处理
			  response.setHeader('content-type', 'text/html;charset=utf-8')
			  # 设置状态码
			  res.statusCod = 200
			  # 向客户端发送响应数据,并结束本次请求的处理过程
			  res.end('hello world')
④、启动服务
	在window中查看当前电脑中是否占用此端口命令
	在window中查看当前电脑中是否占用此端口命令
	 如果返回为空，表示此端口没有表占用，有值则为占用
	 window  netstat -ano | findstr 8080 
	 mac      sudo lsof -i tcp:port
	```

```javascript
server.listen(8080, '0.0.0.0' () => {
console.log('服务已启动')
})	
```

#### 3、静态资源服务器

```javascript
        /** 拼接出来没问题，但是不能确定文件存不存在
         * so...
         * 判断文件是否存在
         * */ 

        if(fs.existsSync(filepath)){ //存在
            // 设置响应头中的mime类型
            res.setHeader('content-type' , mime)
            // 读取磁盘中存在的文件  显示
            res.end(fs.readFileSync(filepath))
        } else { //不存在
            // 404页面路径
            var filepath = path.join(webRoot,'/404.html')
            
            // 设置响应头中的mime类型
            res.setHeader('content-type' , mime)

            //响应状态码
            res.statusCode = 404
            // 服务器对客户端本次请求的响应
            // 404页面
            res.end(fs.readFileSync(filepath))
        }
```



#### 4、浏览器缓存

如果两者同时存在，则以强缓存为主，只有强缓存不存在，或强缓失效，则以协商缓存

强缓存：设置简单，但是如果服务器端文件有修改，客户端在没有达到过期时间，它不会更新文件。 如果有文件更新，但是有强缓存，在前期写文件名称时，加上hash值，只要文件有修改，让对应的文件名称修改，这样达到，修改后，文件能第1时间在客户端显示

强缓存返回状态码：200

例： index.css =>  index-a23432b11.css => index.css?v=1

```javascript
//设置强缓存
res.setHeader('expires',new Date(Date.now() + 3600 * 1000).toGMTString())
res.setHeader('cache-control','max-age=' + 3600)
```



协商缓存：如果文件没有修改，则读缓存，文件有修改,则读最新文件，并缓存起来

有缓存：304，无200

```javascript
let mtime = fs.statSync(filepath).mtime.toGMTString()                
let lastTime = req.headers['if-modified-since']
console.log(mtime,lastTime);
               
if(mtime = lastTime) {
    res.statusCode = 304
    res.end('')
    return;
}
```



#### 5、get数据获取

get数据通过地址栏使用query方式进行传递的数据 例?id=1&name=zhangsan

先获取当前url地址

```javascript
http.createServer((req,res) => {
    
    if('/favicon.ico' != req.url) {
        //true参数可以让query（/?id=1&name=zhangsan）变成对象
        //pathname 和 query是Url对象中的两个属性
        let { pathname , query } = url.parse(req.url , true);
		//得到get数据  这里就是请求参数
        console.log(query) //{id:'100',name:'lisi'}
		//给request对象添加一个自定义的属性
        //当项目做大的时候，get在任何地方通过req.query 都能得到
        req.query = query
    }

    res.end('aaa')

}).listen(3000 , '127.0.0.1' , () => console.log('服务器启动'))
```

res.end()中只能传两种格式：

1、buffer数据流二进制 Buffer from('aaa')

2、字符串 JSON.stringify({id : 1})





#### 6、post数据获取

表单数据多数为post进行提交到服务器端

POST数据  是以流的形式进行数据传递

POST分为两种：标准型  数据流型  一种能上传文件  一种不能上传文件

标准型  只能接受非文件上传的图信息

```javascript
http.createServer((req,res) => {

    if('/favicon.ico' != req.url) {
		//确定是POST请求
        if(req.method == 'POST') {
            let postData = ''
			//在nodejs中流，用事件来完成数据接收
            //在这个事件中 会得到一个chunk（buffer）
            //来一点  加一点  
            //接收数据中  
            req.on('data' , chunk => {
                postData += chunk
            })
			//此事件触发  表示 数据 接收完毕  
            req.on('end', () => {
                console.log(qs.parse(postData));
            })
        }
    }
    res.end('aaab');

}).listen(3000,'127.0.0.1' , () => console.log('服务器启动'))
```

有/？的用url解析

没有/? 的用querystring解析

Postman软件使用仿真

#### 7、文件上传

异步文件上传，通过文件流方式进行文件上传

html5中提供一个js的api方法 ，得到文件域中的对象 File

```javascript
http.createServer((req , res) => {
    
    res.send = function (data) {
        
    }
})
```







