const http = require('http')
const path = require('path')
const fs = require('fs')
const crypto = require('crypto')
const server = http.createServer();

let mimeTypes = require('./libs/mime')
const webRoot = path.resolve('public')

server.on('request',(req,res) => {
    let pathname = req.url == '/' ? '/index.html' : req.url
    let extname = path.extname(pathname) ? path.extname(pathname).slice(1) : ''

    if('' == extname) {
        res.setHeader('content-type' , 'application/json');
        res.end(JSON.stringify({
            id : 1,
            name : '张三'
        }))
    }else {
        if( pathname != '/favicon.ico') {
            var filepath = path.join(webRoot , pathname )
            let mime = mimeTypes[extname] || 'text/html;charset=utf-8'

            if(fs.existsSync(filepath)){
                res.setHeader('content-type' , mime)
                res.end(fs.readFileSync(filepath))
            } else {
                var filepath = path.join(webRoot, '/404.html')
                res.setHeader('content-type' , mime)
                res.statusCode = 404
                res.end(fs.readFileSync(filepath))
            }
        }
    }
});

/** 未渲染列表 */

server.listen(3000, '127.0.0.1', () => console.log('服务器启动'))

