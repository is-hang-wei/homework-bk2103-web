#	NodeJs_01
- 目标
	- 能够命令中执行node程序
	- 能够使用npm安装第三方模块
	- 掌握常用的nodejs内置模块
	- 能够实现文件读写操作
	- web服务器基础知识

## 一、Nodejs概述
### 1.1	介绍
- 基于Google V8引擎
- 单进程
- 异步

- 注意：
    - 浏览器是JS的前端运行环境。
    - 无法调用 DOM 和 BOM 等浏览器内置 API。
    - Node.js可以使用官方API编写出web服务，运行web应用。

### 1.2	Nodejs应用场景

- 创建应用服务
- web静态资源服务和接口服务器
- 客户端应用工具	gulp webpack vue脚手架	react脚手架

### 1.3	安装Nodejs

- nvm可以让你的电脑中有N多个node版本

### 1.4、npm包管理器

- 1.4.1、介绍
	- 查看版本	nmp -v

- 1.4.2、切换npm源 
	- 安装	cnpm i nrm -g
	- 查看可用源		nrm ls
	- 切换可用源		nrm use 名称(npm)

- 1.4.3、生成JSON配置文件

	- npm init -y[不询问]

	- 参数含义
	
		- packer name      包名
    - version          版本
    - description      描述
    - main             入口文件
    - scripts          支持的脚本，默认是一个空的 test
    - keywords         关键字，有助于在人们使用 npm search 搜索时发现你的项目
    - author           作者
    - license:         版权许可证（默认：ISC）
    - dependencies     在生产环境中需要用到的依赖
    - devDependencies  在开发、测试环境中用到的依赖

- 1.4.4、查看当前安装的树形模块
	- 查看本项目已安装模块	npm list
	- 查看包可用版本	npm view jquery versions
	
- 1.4.5、安装模块	npm install 模块名[@版本号  可选]  或  npm i 模块名[@版本号  可选]

- 1.4.6、自定义脚本命令
	- 通过package.json文件中的scripts自定义脚本命令
```json
	{
		"scripts" : {
			"test" : "echo hello"
		}
	}
```

- 运行命令
	- npm run test
	
	![image-20210531185636148](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531185636148.png)

![image-20210531185611441](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531185611441.png)

- 1.4.7、自动重启应用

	- 工具：nodemon
	- 作用：监听代码文件的变动，当代码改变之后，自动重启
### 1.5、模块化
- NodeJs是基于Commonjs模块化开发的规范

- 定义一个JS文件就称为一个模块

- node的模块类型

	- 核心模块
	- 第三方模块
	- 自定义模块
	
- 导出	module.exports/exports

  - 下面两种等价，导出是一个对象
	exports.fn = fn
	module.exports.fn = fn
  - 下面这种写法导出是一个函数
	module.exports = fn
	- ![image-20210531200932454](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531200932454.png)


- 导入	require(单例模式，导入相同的对象，全局只有一个实例)

![image-20210531200955569](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531200955569.png)





问题：

![image-20210531211849315](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531211849315.png)

![image-20210531211910568](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531211910568.png)

class也是一个function，怎么理解？

## 常用内置模块
### 2.1、os模块
- os模块提供了与操作系统相关的实用方法和属性

  ![image-20210531201814890](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531201814890.png)

![image-20210531201849682](C:\Users\30210\AppData\Roaming\Typora\typora-user-images\image-20210531201849682.png)


### 2.2、path模块
- path模块用于处理文件和目录（文件夹）的路径
- 语法

	- // 获取路径模块
	const path = require('path');
	- path.basename(dirpath)   得到路径中最后一个分隔符后面的内容
	- path.dirname(dirpath)    得到路径中最后一个分隔符前面的字符串
	- console.log(__dirname)   得到当前执行文件的绝对根目录
	- console.log(__filename)  得到当前执行文件的进而对路径
	- path.extname(dirpath)    获取路径中文件扩展名
	- path.join('f','www','a.html') 给定的路径连接在一起
	- path.resolve('../data/user.json')    相对地址转化成绝对地址
	
	
### 2.3、url模块
- URL字符串是结构化的字符串，包含多个含义不同的组成部分。
- 其中，比较重要的是pathname和search
- const url = require('url');
- const href = '//www.so.com/s?id=1000#hash'
- const urlObj = url.parse(href , true , true )
- console.log(urlObj.pathname)     /s
- console.log(urlObj.query.id)	1000
- console.log(urlObj)
- Url {                                   
  protocol: null,                       
  slashes: true,                        
  auth: null,                           
  host: 'www.so.com',                   
  port: null,                           
  hostname: 'www.so.com',               
  hash: '#hash',                        
  search: '?id=1000',                   
  query: { id: '1000' },                
  pathname: '/s',                       
  path: '/s?id=1000',                   
  href: '//www.so.com/s?id=1000#hash' } 
- 解析得到url对象
- 
- 参数1：url地址
- 参数2：对于返回对象中query属性是以对象返回还是字符串，false字符串，true对象
- 参数3：在没有写协议时，进行的域名解析，模式是false,在没有写协议时，可以设置为true
- 

