const fn = () => console.log('我是fn');

const username = '杭威'
// const age = 24

class User {
    run(){
        console.log('仲夏夜之梦');
    }
}

// // 导出
module.exports = {
    fn,
    username,
    user : new User
}

// const fnn = () => console.log("+666")
// exports.fnn = fnn
// module.exports.fnn = fnn;