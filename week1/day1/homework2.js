const path = require('path')
const fs = require('fs')

function rmdir( filepath ){
    // fs.stat(filepath , (err , stat) => {
    //     if(stat.isDirectory()){

    //     }
    // })
    if(fs.existsSync(filepath)){
        fs.readdirSync(filepath).forEach(function(file){
            var curPath = filepath + "/" + file;
            if(fs.statSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { 
                // 删除
                fs.unlinkSync(curPath);
            }
        })
    }
}

