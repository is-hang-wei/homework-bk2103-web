const path = require('path');
const os = require('os');
const fs = require('fs');
const pfs = require('fs').promises;
const { resolve } = require('path');
const { rejects } = require('assert');

// const filepath = path.resolve('./fs.txt');

// fs.readFile(filepath , (err,buffer) => {
//     if(!err) {
//         console.log(buffer.toString());
//         console.log('成功');

//     } else{
//         console.log('失败');
//     }
// })

// 同步
// console.log(fs.readFileSync(filepath , 'utf-8'));







// function readFile(filepath) { 
//     return new Promise((resolve,reject) => {
//         fs.readFile(filepath , 'utf-8' , (err,data) => {
//             if(!err){
//                 resolve(data)
//             } else{
//                 reject(err)
//             }
//         })
//     })
//  }

 // readFile(filepath).then(res => console.log(res))
// pfs.readFile(filepath,'utf-8').then(res => console.log(res));


// ;(async function (filepath) {
//     let res = await readFile(filepath)
//     console.log("===" ,res );
// })(filepath)

// 覆盖删除
// const filepath = path.resolve('./fs.txt');

// fs.writeFile(filepath , '12853' + os.EOL , {
//     flag:'w'
// },err => {
//     console.log(err);
// })

// 追加删除
// fs.appendFile(filepath , 'abcfeww' + os.EOL , err => {
//     console.log(err);
// })

// const filepath = path.resolve('./1.txt');

// 删除
// fs.unlink(filepath, () => {
//     console.log(1);
// })

// console.log(fs.existsSync(filepath))
// console.log(fs.existsSync(filepath))

// 查看当前文档信息
// fs.stat(filepath , (err , stat) => {
//     console.log(stat.isDirectory());
//     console.log(stat.isFile())

//     // 文件修改时间
//     console.log(stat.mtime);

//     console.log(stat.size);
// })


// const filepath = path.resolve('./modules');

// 读目录
// fs.readdir(filepath , ( err , files ) => {
//     files.forEach(file => {
//         fs.stat(path.join(filepath ,file),(err, stat) => {
//             if(stat.isFile()){
//                 console.log(path.join(filepath,file) + '==文件')
//             } else {
//                 console.log(path.join(filepath , file) + '==目录');
//             }
//         })
//     })
// })

const filepath = path.resolve('./data');

fs.rmdirSync(filepath , {recursive:true})