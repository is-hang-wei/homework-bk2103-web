const href = 'https://www.so.com/s?id=1000&name=lisi'

// js（注意）提供一个URL类，它可以直接解析url
const url = new URL(href);

// console.log(url.pathname);

// console.log(url.searchParams.get('id'));

// console.log(url.searchParams.get('name'));

// for(let item of url.searchParams.entries()){
//     console.log(item);
// }

// 结构赋值
for(let [key,value] of url.searchParams.entries()){
    console.log(key , value);
}
