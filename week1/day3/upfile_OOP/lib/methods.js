const fs = require('fs')
const path = require('path')

// 模块导出
module.exports = {
    upfile(req,res) {

        let data = []
        req.on('data' , chunk => data.push(chunk))
        req.on('end' , () => {
            let filename = Date.now() + path.extname(req.headers['filename'])

            let filepath = path.resolve('www/uploads/' + filename)
            fs.writeFileSync(filepath , Buffer.concat(data))
            res.send({ code : 0 , msg : 'ok' ,url : '/uploads/' + filename })
        })
    }
}