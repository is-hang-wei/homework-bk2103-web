# NodeJs_03

#### 目标

Ø 完成异步文件上传

Ø 能够编写运行express静态服务器

Ø 能够在express中获取get和url路由参数 - 路由



#### 1、文件上传

异步文件上传，通过文件流方式进行文件上传

html5中提供一个js的api方法 ，得到文件域中的对象 File

```javascript
const { join, extname } = require('path')
const fs = require('fs')
const http = require('http')
const url = require('url')
const qs = require('querystring')
const mimes = require('./libs/mime')

// 网站根目录  url地址中的 / => www目录
const webRoot = join(__dirname, 'www')

// get数据获取
http.createServer((req, res) => {
  let { pathname, query } = url.parse(req.url, true)
  // post处理
  if (req.method === 'POST') {
    // 路由 post登录处理  流
    if (pathname == '/upload') {
      // 文件名称
      let filename = Date.now() + extname(req.headers.filename);
      // 实现文件上传
      req.pipe(fs.createWriteStream(join(webRoot, 'uploads', filename)))
      res.end(JSON.stringify({ code: 0, url: 'http://localhost:3000/uploads/' + filename }))
    }

  } else {
    if (req.url != '/favicon.ico') {
      // query，就是浏览器访问的get参数集合
      pathname = pathname === '/' ? '/index.html' : pathname
      // 得到请求的对象服务器中真实的文件路径
      let filepath = join(webRoot, pathname)

      if (fs.existsSync(filepath)) {
        // 得到请求文件的扩展名
        const ext = extname(filepath).slice(1)
        // 得到扩展名
        const mime = mimes[ext]
        // 设置响应头
        res.setHeader('content-type', mime)
        // res.setHeader('Content-Encoding', 'gzip')
        let html;
        if ('html' === ext) {
          html = fs.readFileSync(filepath, 'utf-8');
          // html = html.replace('{{id}}', query.id)
          html = html.replace(/\{\{\s*(\w+)\s*\}\}/g, (preg, match) => {
            return query[match]
          })
        } else {
          html = fs.readFileSync(filepath)
        }
        res.end(html)
      } else {
        res.statusCode = 404
        res.end('not found')
      }
    }
  }
}).listen(3000, '0.0.0.0')

```



#### 2、Express

安装

```javascript
npm init -y
npm i -S express
```

创建web服务

```javascript
const express = require('express')
// 创建web服务
const app = express()

// 监听 get请求
// req 请求对象
// res 响应对象
app.get('请求URI',(req,res)=>{
	// 向客户端响应数据
	res.send({id:1,name:'张三'})
})
// 启动服务
app.listen(8080,()=>{})

```

获取URL中get查询参数

```javascript
app.get('/',(req,res)=>{
	console.log(req.query)
})
```

URL中动态参数

```javascript
app.get('/:uid',(req,res)=>{
	console.log(req.params)
})
? 可选
```

静态资源管理

express提供了一个非常好用的方法，叫做 express.static()，通过此方法，可以非常方便地创建一个静态web资源服务器

```javascript
app.use(express.static('public'))
现在可以访问public目录下所有的文件 
如public/aa.jpg文件，则可以通过 : http://xxxx/images/aa.jpg

挂载路径前缀,希望是访问到指定的路径后才触发到静态资源管理
app.use('public', express.static('public'))
如public/aa.jpg文件，则可以通过 : http://xxxx/public/images/aa.jpg

```



