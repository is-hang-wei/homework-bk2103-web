//www就是 静态资源根目录
const path = require('path')
const fs = require('fs')
const http = require('http')
const url =  require('url')



// 网站根目录
const webRoot = path.resolve('www')

//操作动态对象引入
const action = require('./lib/action')

//创建  web服务器
http.createServer((req,res) => {
    // console.log(req,res);
    // 声明send方法
    res.send = function (data) {
        if ( typeof data  == 'object' ) {
            //设置请求头
            res.setHeader('content-type','application/json')
            // 将传来的data转换成json字符串
            data = JSON.stringify(data)
        }
        res.setHeader('content-type','text/html;charset=utf-8')
        res.end(data)
    }

    //取出浏览器icon 请求
    if ('/favicon.ico' != req.url) {
        //解析url路径
        let {pathname ,query} = url.parse(req.url , true)
        pathname = pathname == '/' ? '/index.html' : pathname
        
        //判断是否是接口还是静态资源
        // 获取路径中文件扩展名
        let extname = path.extname(pathname)
        if(extname) { //静态资源
            //请求的真实的文件磁盘路径
            let filepath = path.join(webRoot , pathname )

            //判断文件是否存在
            if( fs.existsSync(filepath)) { //文件存在                 
                // 读取路径中的文件内容并输出
                res.end(fs.readFileSync(filepath))
            } else { //文件不存在
                // 响应状态码
                res.statusCode = 404
                // 输出html结构
                res.end(`
                    <script>document.write('\
                        <ul>\
                            <li>热门商品</li>\
                            <li>推荐商品</li>\
                            <li>在线客服</li>\
                        </ul>\
                    ')</script>
                `)
            }
           
        } else { //接口 
            // 判断请求方式是不是POST请求
            if(req.method == 'POST') {
                 
                //异常捕获
                try {
                    action[pathname.slice(1)](req,res)
                } catch (error) {
                    res.statusCode = 404
                    res.send({ code:1000,msg:'404'})
                }

                // // 得到方法去掉斜线的名字
                // methodName = pathname.slice(1)
                // // 异常捕获
                // try {
                //     //执行方法
                //     method[methodName](req,res)
                // } catch (e) {
                //     // 响应状态码
                //     res.statusCode = 500;
                //     // 调用send方法
                //     res.send({ code : 1000 , msg : '服务器内部问题' })
                // }
            } else if(req.method == 'GET') {
                // if(pathname == '/556'){
                //     res.send('登录成功le')
                // } else{
                //     res.send('404')
                // }
                // switch (pathname) {
                //     case '/login' :
                //     return res.send('行者杭到此一游');
                //     default:
                //         res.send('查无此人');
                // }

               


                 
            }
        }
    }
}).listen(3000 , '0.0.0.0' , () => console.log('server start... '))

