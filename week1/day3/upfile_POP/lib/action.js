const fs = require('fs')
const path = require('path')
const qs =require('querystring')
const url = require('url')

// //像这样在对象中定义方法
// const obj = {
//     name : '张三',
//     fn : function(){
//         console.log(this);
//     }
// }

// //使用箭头函数
// const obj = {
//     name = '张三',
//     fn : () => {
//         console.log(this);
//     }
// }

// 在ES6中函数可以改写成这种写法
// const obj = {
//     name : '张三',
//     fn(){
//         console.log(this);
//     }
// }

// const obj = {
//     name : '张三',
//     fn(){
//         console.log(this)
//     }
// }

// const obj = {
//     name : '张三',
//     fn(){
//         console.log(this)
//     }
// }

// obj[key]()


module.exports = {
    //第一个方法
    login(req , res){
        res.send('登录一下')
    },

    upfile(req,res) {
        let filename = Date.now() + path.extname(req.headers['filename'])
        let filepath = path.resolve('www/uploads/' + filename)

        //可读流
        //可写流
        let ws = fs.createWriteStream(filepath)
        req.pipe(ws)

        //可写流
        ws.on('finish',()=>{
            res.send({ code: 0 , msg: 'ok', url: '/uploads/' + filename})
            
        } )
    },

    //删除图片
    delpic(req,res) {
        let data = []
        req.on('data' , chunk => data.push(chunk))
        req.on('end' , () => {
            let { pathname: postData } = qs.parse(Buffer.concat(data).toString())
            let umgPathObj = url.parse(postData , true)
            //图片在服务器中真实的路径
            let filepath = path.join(__dirname , '../www' , imgPathObj.pathname)

            //判断路径下面有没有此文件，如果没有则不处理、
            if (fs.existsSync(filepath)) {
                //文件存在 同步方法
                fs.unlinkSync(filepath);
            }
            res.send({ code : 0 ,msg : 'ok' })
        })
    }
}
